<?PHP // $Id$

$string['errbadxmlformat'] = 'Error - bad XML format';
$string['errduplicateidnumber'] = 'Error - duplicate idnumber';
$string['errincorrectidnumber'] = 'Error - incorrect idnumber';
$string['fileurl'] = 'Remote file URL';
$string['modulename'] = 'XML file';
$string['xml:view'] = 'Import grades from XML';
$string['xml:publish'] = 'Publish import grades from XML';

?>
