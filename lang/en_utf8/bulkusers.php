<?php //$Id$
$string['usersinlist'] = 'Users in list';
$string['addall'] = 'Add all available users';
$string['addsel'] = 'Add selected available users';
$string['removeall'] = 'Remove all available users';
$string['removesel'] = 'Remove selected available users';
$string['deleteall'] = 'Clear all users';
$string['deletesel'] = 'Clear selected users';
$string['available'] = 'Available';
$string['selected'] = 'Selected';
$string['selectedlist'] = 'Selected user list...';
$string['usersfound'] = '$a user(s) found.';
$string['usersselected'] = '$a user(s) selected.';